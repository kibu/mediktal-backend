package com.kibu.mediktalbackend.repository;

import com.kibu.mediktalbackend.model.Bed;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public interface IBedRepository {
    List<Bed> listBeds();

    Optional<Bed> findBed(String id);

    void save(Bed bed);
}
