package com.kibu.mediktalbackend.repository;

import com.kibu.mediktalbackend.model.Bed;
import org.mongodb.morphia.Datastore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class BedRepositoryImpl implements IBedRepository {

    private static final String BED_ID = "bedId";

    private Datastore datastore;

    @Autowired
    public BedRepositoryImpl(Datastore datastore) {
        this.datastore = datastore;
    }

    @Override
    public List<Bed> listBeds() {
        return datastore.find(Bed.class).asList();
    }

    @Override
    public void save(Bed bed) {
        datastore.save(bed);
    }

    @Override
    public Optional<Bed> findBed(String id) {
        return Optional.ofNullable(datastore.find(Bed.class).field(BED_ID).equal(id).get());
    }
}
