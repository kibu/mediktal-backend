package com.kibu.mediktalbackend;

import com.kibu.mediktalbackend.model.Bed;
import com.kibu.mediktalbackend.model.Note;
import com.kibu.mediktalbackend.model.PatientDataSheet;
import com.kibu.mediktalbackend.model.Todo;
import com.kibu.mediktalbackend.repository.IBedRepository;
import com.kibu.mediktalbackend.request.NewNoteRequest;
import com.kibu.mediktalbackend.request.NewPatientDataSheetRequest;
import com.kibu.mediktalbackend.request.NewTodoRequest;
import com.kibu.mediktalbackend.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
@RequestMapping("/beds")
public class BedController {

    @Autowired
    IBedRepository bedRepository;

    @GetMapping
    public ListBedsResponse listBeds() {
        List<Bed> beds = bedRepository.listBeds();
        return ListBedsResponse.fromBedList(beds);
    }

    @GetMapping(value = {"/{id}/empty"})
    public EmptyBedResponse emptyBed(@PathVariable(name = "id") String bedid) {
        Bed bed = bedRepository.findBed(bedid).orElseThrow(() -> new NoSuchElementException(String.format("Bed not found with id: %s", bedid)));
        bed.emptyBed();
        bedRepository.save(bed);
        return EmptyBedResponse.fromBed(bed);
    }

    @GetMapping(value = {"/{id}"})
    public BedResponse getBedById(@PathVariable(name = "id") String bedid) {
        Bed bed = bedRepository.findBed(bedid).orElseThrow(() -> new NoSuchElementException(String.format("Bed not found with id: %s", bedid)));
        return BedResponse.fromBed(bed);
    }

    @PostMapping(value = {"/{id}/data"})
    public NewPatientDataSheetResponse newPatientDataSheet(@PathVariable(name = "id") String bedid, @RequestBody @Valid NewPatientDataSheetRequest newPatientDataSheetRequest) {
        Bed bed = bedRepository.findBed(bedid).orElseThrow(() -> new NoSuchElementException(String.format("Bed not found with id: %s", bedid)));
        PatientDataSheet patientDataSheet = PatientDataSheet.fromRequest(newPatientDataSheetRequest);
        bed.setPatientDataSheet(patientDataSheet);
        bedRepository.save(bed);
        return new NewPatientDataSheetResponse(patientDataSheet.getName());
    }

    @GetMapping(value = {"/{id}/todos"})
    public List<TodoResponse> getTodos(@PathVariable(name = "id") String bedid, @RequestBody NewTodoRequest newTodoRequest) {
        Bed bed = bedRepository.findBed(bedid).orElseThrow(() -> new NoSuchElementException(String.format("Bed not found with id: %s", bedid)));
        return bed.getTodos().stream().map(TodoResponse::fromTodo).collect(Collectors.toList());
    }

    @PostMapping(value = {"/{id}/todos"})
    public TodoResponse newTodo(@PathVariable(name = "id") String bedid, @RequestBody NewTodoRequest newTodoRequest) {
        Bed bed = bedRepository.findBed(bedid).orElseThrow(() -> new NoSuchElementException(String.format("Bed not found with id: %s", bedid)));
        Todo todo = Todo.fromRequest(newTodoRequest);
        bed.addTodo(todo);
        bedRepository.save(bed);
        return TodoResponse.fromTodo(todo);
    }

    @PutMapping(value = {"/{id}/todos/{todoId}"})
    public TodoResponse updateTodo(@PathVariable(name = "id") String bedId, @PathVariable(name = "todoId") String todoId, @RequestBody @Valid NewTodoRequest newTodoRequest) {
        Bed bed = bedRepository.findBed(bedId).orElseThrow(() -> new NoSuchElementException(String.format("Bed not found with id: %s", bedId)));
        Todo todo = bed.getTodos().stream()
                .filter(t -> t.getId().toString().equals(todoId)).findAny()
                .orElseThrow(() -> new NoSuchElementException(String.format("Todo not found with id: %s", todoId)));
        todo.updateFromRequest(newTodoRequest);
        bedRepository.save(bed);
        return TodoResponse.fromTodo(todo);
    }

    @DeleteMapping(value = {"/{id}/todos/{todoId}"})
    public void deleteTodo(@PathVariable(name = "id") String bedId, @PathVariable(name = "todoId") String todoId) {
        Bed bed = bedRepository.findBed(bedId).orElseThrow(() -> new NoSuchElementException(String.format("Bed not found with id: %s", bedId)));
        Todo todo = bed.getTodos().stream()
                .filter(t -> t.getId().toString().equals(todoId)).findAny()
                .orElseThrow(() -> new NoSuchElementException(String.format("Todo not found with id: %s", todoId)));
        bed.removeTodo(todo);
        bedRepository.save(bed);
    }

    @GetMapping(value = {"/{id}/todos/{todoId}/done"})
    public MakeTodoDoneResponse makeTodoDone(@PathVariable(name = "id") String bedId, @PathVariable(name = "todoId") String todoId) {
        Bed bed = bedRepository.findBed(bedId).orElseThrow(() -> new NoSuchElementException(String.format("Bed not found with id: %s", bedId)));
        Todo todo = bed.getTodos().stream()
                .filter(t -> t.getId().toString().equals(todoId)).findAny()
                .orElseThrow(() -> new NoSuchElementException(String.format("Todo not found with id: %s", todoId)));
        todo.makeTodoDone();
        bedRepository.save(bed);
        return MakeTodoDoneResponse.fromTodo(todo);
    }

    @PostMapping(value = {"/{id}/notes"})
    public NewNoteResponse addNewNote(@PathVariable(name = "id") String bedid, @RequestBody @Valid NewNoteRequest noteRequest) {
        Bed bed = bedRepository.findBed(bedid).orElseThrow(() -> new NoSuchElementException(String.format("Bed not found with id: %s", bedid)));
        Note note = Note.fromNewNoteRequest(noteRequest);
        bed.addNote(note);
        bedRepository.save(bed);
        return NewNoteResponse.fromNote(note);
    }
}
