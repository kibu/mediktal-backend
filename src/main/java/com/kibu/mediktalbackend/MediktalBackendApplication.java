package com.kibu.mediktalbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MediktalBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MediktalBackendApplication.class, args);
	}

}
