package com.kibu.mediktalbackend.model;

import com.kibu.mediktalbackend.request.NewNoteRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.time.LocalDateTime;
import java.time.ZoneId;

@Data
@AllArgsConstructor
@Entity
public class Note {
    @Id
    private ObjectId id;
    private String text;
    private LocalDateTime lastUpdated;

    public Note() {
    }

    public static Note fromNewNoteRequest(NewNoteRequest newNoteRequest) {
       return new Note(new ObjectId(), newNoteRequest.getText(), LocalDateTime.now(ZoneId.systemDefault()));
    }
}
