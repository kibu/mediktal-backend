package com.kibu.mediktalbackend.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@Entity(noClassnameStored = true)
@Indexes(@Index(fields = {@Field("bedId")}))
public class Bed {

    @Id
    private ObjectId id;
    private String bedId;
    private int floor;
    private int roomId;
    private String hostpitalClassName;
    private int position;
    private String groupName;
    private String hospitalName;
    @Embedded
    private PatientDataSheet patientDataSheet;
    @Embedded
    private List<Note> notes;
    @Embedded
    private List<Todo> todos;
    private LocalDateTime bedEmptied;

    public Bed() {
        this.notes = new ArrayList<>();
        this.todos = new ArrayList<>();
        this.hospitalName = "Szent János Kórház";
    }

    public boolean isOccupied() {
        return patientDataSheet != null;
    }

    public void addTodo(Todo todo) {
        if (todo != null) {
            this.todos.add(todo);
        }
    }

    public void removeTodo(Todo todo) {
        todos.remove(todo);
    }

    public void addNote(Note note) {
        if (note != null) {
            this.notes.add(note);
        }
    }

    public void emptyBed() {
        this.notes = new ArrayList<>();
        this.todos = new ArrayList<>();
        this.patientDataSheet = null;
        this.bedEmptied = LocalDateTime.now(ZoneId.systemDefault());
    }

    @PrePersist
    public void setBedId() {
        this.bedId = id.toHexString();
    }

}
