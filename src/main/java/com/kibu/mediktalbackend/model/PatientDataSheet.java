package com.kibu.mediktalbackend.model;

import com.kibu.mediktalbackend.request.NewPatientDataSheetRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.time.LocalDate;

@Data
@Builder
@Entity
@AllArgsConstructor
public class PatientDataSheet {
    @Id
    private ObjectId id;
    private String name;
    private int age;
    private LocalDate intakeDate;
    private String hospital;

    private String diagnosis;
    private String previousDiseases;
    private String consciousness;

    private String breathing;
    private String circulation;
    private String diuresis;
    private String abdominalStatus;

    private String breedingResults;
    private String antibiotics;

    private String other;

    public PatientDataSheet() {
    }

    public static PatientDataSheet fromRequest(NewPatientDataSheetRequest newPatientDataSheetRequest) {
        return PatientDataSheet.builder().name(newPatientDataSheetRequest.getName())
                .age(newPatientDataSheetRequest.getAge())
                .abdominalStatus(newPatientDataSheetRequest.getAbdominalStatus())
                .antibiotics(newPatientDataSheetRequest.getAntibiotics())
                .breathing(newPatientDataSheetRequest.getAntibiotics())
                .breedingResults(newPatientDataSheetRequest.getBreedingResults())
                .circulation(newPatientDataSheetRequest.getCirculation())
                .consciousness(newPatientDataSheetRequest.getConsciousness())
                .diagnosis(newPatientDataSheetRequest.getDiagnosis())
                .diuresis(newPatientDataSheetRequest.getDiuresis())
                .hospital(newPatientDataSheetRequest.getHospital())
                .intakeDate(newPatientDataSheetRequest.getIntakeDate())
                .previousDiseases(newPatientDataSheetRequest.getPreviousDiseases())
                .other(newPatientDataSheetRequest.getOther()).build();
    }
}
