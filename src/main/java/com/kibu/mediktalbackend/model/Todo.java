package com.kibu.mediktalbackend.model;

import com.kibu.mediktalbackend.request.NewTodoRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.time.LocalDateTime;
import java.time.ZoneId;

@Data
@AllArgsConstructor
@Entity
public class Todo {

    @Id
    private ObjectId id;
    private String text;
    private boolean isDone;
    private LocalDateTime lastUpdated;

    public Todo() {
    }

    public static Todo fromRequest(NewTodoRequest newTodoRequest) {
        return new Todo(new ObjectId(), newTodoRequest.getText(), false, LocalDateTime.now(ZoneId.systemDefault()));
    }

    public Todo updateFromRequest(NewTodoRequest newTodoRequest) {
        if (newTodoRequest.getText() != null) {
            this.setText(newTodoRequest.getText());
            this.setLastUpdated(LocalDateTime.now(ZoneId.systemDefault()));
        }
        return this;
    }

    public Todo makeTodoDone() {
        this.setDone(true);
        this.lastUpdated = LocalDateTime.now(ZoneId.systemDefault());
        return this;
    }
}
