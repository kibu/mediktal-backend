package com.kibu.mediktalbackend.request;

import lombok.Data;

@Data
public class NewTodoRequest {
    private String text;
}
