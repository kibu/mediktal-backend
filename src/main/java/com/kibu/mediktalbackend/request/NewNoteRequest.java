package com.kibu.mediktalbackend.request;

import lombok.Data;

@Data
public class NewNoteRequest {
    private String text;
}
