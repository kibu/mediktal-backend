package com.kibu.mediktalbackend.request;

import lombok.Data;

import java.time.LocalDate;

@Data
public class NewPatientDataSheetRequest {
    private String name;
    private int age;
    private LocalDate intakeDate;
    private String hospital;

    private String diagnosis;
    private String previousDiseases;
    private String consciousness;

    private String breathing;
    private String circulation;
    private String diuresis;
    private String abdominalStatus;

    private String breedingResults;
    private String antibiotics;

    private String other;


    public int getAge() {
        return this.age == 0 ? -1 : this.age;
    }

}
