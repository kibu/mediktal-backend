package com.kibu.mediktalbackend.db;

import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MongoConfiguration {

    @Value("${mongo.database}")
    private String database;
    @Value("${mongo.host}")
    private String host;
    @Value("${mongo.port}")
    private int port;

    public @Bean
    Datastore datastore() {
        Morphia morphia = new Morphia();
        morphia.mapPackage("com.kibu.mediktalbackend.model");
        ServerAddress serverAddress = new ServerAddress(this.host, this.port);
        Datastore datastore = morphia.createDatastore(new MongoClient(serverAddress), this.database);
        datastore.ensureIndexes();
        return datastore;

    }


}
