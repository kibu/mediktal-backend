package com.kibu.mediktalbackend.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
public class ListBedsResponse {

    private Hospital hospital;

    public static ListBedsResponse fromBedList(List<com.kibu.mediktalbackend.model.Bed> beds) {
        return new ListBedsResponse(Hospital.fromBedList(beds));
    }

    @Data
    @AllArgsConstructor
    static class Hospital {
        private String name;
        private List<Bed> beds;

        public static Hospital fromBedList(List<com.kibu.mediktalbackend.model.Bed> beds) {
            List<Bed> bedList = beds.stream().map(Bed::fromModelBed).collect(Collectors.toList());
            return new Hospital("Szent János Kórház", bedList);
        }
    }

    @Data
    @AllArgsConstructor
    static class Bed {
        private String bedId;
        private String className;
        private int floor;
        private int position;
        private String groupName;
        private boolean occupied;
        private String data;

        static Bed fromModelBed(com.kibu.mediktalbackend.model.Bed bed) {
            String name = null;
            if (bed.getPatientDataSheet() != null) {
                name = bed.getPatientDataSheet().getName();
            }

            return new Bed(bed.getBedId(),
                    bed.getHostpitalClassName(),
                    bed.getFloor(),
                    bed.getPosition(),
                    bed.getGroupName(),
                    bed.isOccupied(),
                    name);
        }
    }
}

