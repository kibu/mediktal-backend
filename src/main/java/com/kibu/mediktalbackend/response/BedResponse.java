package com.kibu.mediktalbackend.response;

import com.kibu.mediktalbackend.model.Bed;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
public class BedResponse {
    private String bedId;
    private String className;
    private int floor;
    private int room;
    private boolean occupied;
    private int position;
    private String groupName;
    private PatientDataSheet data;
    private List<Todo> todos;
    private List<Note> notes;

    public static BedResponse fromBed(Bed bed) {
        List<Note> notes = new ArrayList<>();
        List<Todo> todos = new ArrayList<>();
        if (bed.getNotes() != null) {
            notes = bed.getNotes().stream().map(Note::fromNote).collect(Collectors.toList());
        }

        if (bed.getTodos() != null) {
            todos = bed.getTodos().stream().map(Todo::fromTodo).collect(Collectors.toList());
        }
        return new BedResponse(
                bed.getId().toString(),
                bed.getHostpitalClassName(),
                bed.getFloor(),
                bed.getRoomId(),
                bed.isOccupied(),
                bed.getPosition(),
                bed.getGroupName(),
                PatientDataSheet.fromDataSheet(bed.getPatientDataSheet()),
                todos,
                notes);
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class PatientDataSheet {
        String name;

        static PatientDataSheet fromDataSheet(com.kibu.mediktalbackend.model.PatientDataSheet patientDataSheet) {
            if (patientDataSheet != null) {
                return new PatientDataSheet(patientDataSheet.getName());
            }
            return new PatientDataSheet();
        }
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class Note {
        String noteId;
        String text;
        LocalDateTime lastUpdated;

        static Note fromNote(com.kibu.mediktalbackend.model.Note note) {
            if (note != null) {
                return new Note(note.getId().toString(), note.getText(), note.getLastUpdated());
            }
            return new Note();
        }
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class Todo{
        String todoId;
        String text;
        LocalDateTime lastDone;

        static Todo fromTodo(com.kibu.mediktalbackend.model.Todo todo) {
            if (todo != null) {
                return new Todo(todo.getId().toString(), todo.getText(), todo.getLastUpdated());
            }
            return new Todo();
        }
    }

}

