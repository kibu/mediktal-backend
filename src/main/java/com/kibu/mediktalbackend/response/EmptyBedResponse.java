package com.kibu.mediktalbackend.response;

import com.kibu.mediktalbackend.model.Bed;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmptyBedResponse {
    private String bedId;
    private String className;
    private int floor;
    private int room;
    private String groupName;
    private boolean occupied;

    public static EmptyBedResponse fromBed(Bed bed) {
        return new EmptyBedResponse(bed.getId().toString(),
                bed.getHostpitalClassName(),
                bed.getFloor(),
                bed.getRoomId(),
                bed.getGroupName(),
                bed.isOccupied());
    }
}
