package com.kibu.mediktalbackend.response;

import com.kibu.mediktalbackend.model.Todo;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TodoResponse {

    private String todoId;
    private String text;

    public static TodoResponse fromTodo(Todo todo) {
        return new TodoResponse(todo.getId().toString(), todo.getText());
    }
}
