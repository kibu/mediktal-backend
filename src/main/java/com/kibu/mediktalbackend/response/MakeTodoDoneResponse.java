package com.kibu.mediktalbackend.response;


import com.kibu.mediktalbackend.model.Todo;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class MakeTodoDoneResponse {
    private String id;
    private String text;
    private LocalDateTime lastDone;

    public static MakeTodoDoneResponse fromTodo(Todo todo) {
        return new MakeTodoDoneResponse(todo.getId().toString(), todo.getText(), todo.getLastUpdated());
    }
}
