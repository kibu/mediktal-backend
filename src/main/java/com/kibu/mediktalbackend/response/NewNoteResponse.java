package com.kibu.mediktalbackend.response;


import com.kibu.mediktalbackend.model.Note;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class NewNoteResponse {
    private String text;
    private LocalDateTime created;

    public static NewNoteResponse fromNote(Note note) {
        return new NewNoteResponse(note.getText(), note.getLastUpdated());
    }
}
