package com.kibu.mediktalbackend.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NewPatientDataSheetResponse {
    String name;
}
