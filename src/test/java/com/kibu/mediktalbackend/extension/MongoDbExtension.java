package com.kibu.mediktalbackend.extension;

import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

public class MongoDbExtension {

    private static final String HOST = "localhost";
    private static final int PORT = 27018;
    private static final String DATABASE = "mediktal-test";

    private static Morphia morphia;
    private static Datastore datastore;

    public static Datastore getMediktalDatastore() {
        if (datastore == null) {
            initializeDatastore();
        }
        return datastore;
    }

    private static Datastore initializeDatastore() {
        morphia = new Morphia();
        morphia.mapPackage("com.kibu.mediktalbackend.model");
        ServerAddress serverAddress = new ServerAddress(HOST, PORT);
        datastore = morphia.createDatastore(new MongoClient(serverAddress), DATABASE);
        datastore.ensureIndexes();
        return datastore;
    }


}
