package com.kibu.mediktalbackend.repository;

import com.kibu.mediktalbackend.extension.MongoDbExtension;
import com.kibu.mediktalbackend.model.Bed;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.mongodb.morphia.Datastore;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BedRepositoryImplTest {

    private Datastore datastore = MongoDbExtension.getMediktalDatastore();
    private IBedRepository bedRepository = new BedRepositoryImpl(datastore);


    @Test
    void testSave() {
        Bed bed = Bed.builder().id(new ObjectId()).bedId("1234").floor(12).hospitalName("Janos Korhaz").build();
        bedRepository.save(bed);
        assertEquals(datastore.find(Bed.class).asList().size(), 1);
    }

    @Test
    void testListBeds() {
        datastore.save(List.of(Bed.builder().id(new ObjectId()).build()));
        List<Bed> beds = bedRepository.listBeds();
        assertEquals(beds.size(), 1);
    }

    @Test
    void testFindBedById() {
        Bed bed = Bed.builder().id(new ObjectId()).bedId("JK12345").floor(12).hospitalName("Janos Korhaz").build();
        datastore.save(bed);

        Optional<Bed> foundBed = bedRepository.findBed("JK12345");
        assertTrue(foundBed.isPresent());
    }

    @AfterEach
    void tearDown() {
        datastore.getDB().dropDatabase();
    }
}
