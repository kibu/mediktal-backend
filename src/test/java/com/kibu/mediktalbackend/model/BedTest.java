package com.kibu.mediktalbackend.model;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class BedTest {

    private Bed bed;

    @Test
    void testEmptyBed() {
        this.bed = Bed.builder().id(new ObjectId()).patientDataSheet(new PatientDataSheet()).notes(List.of(new Note())).build();
        bed.emptyBed();

        assertEquals(bed.getTodos().size(), 0);
        assertFalse(bed.isOccupied());
    }
}
