FROM openjdk:11
ARG JAR_FILE=target/mediktal-backend-1.0.0.war
COPY ${JAR_FILE} app.war
ENTRYPOINT ["java","-jar","/app.war"]
EXPOSE 8080